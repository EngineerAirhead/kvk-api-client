Overheid.io API Client
=========================

Client for calling the [https://api.overheid.io/openkvk](https://api.overheid.io/openkvk) endpoints.

This takes all the extra work of building up the correct url, and requesting the correct endpoints.
Simply by providing a set of specified requests. If a more specific request is required, you can always build a PSR-7 Request object to the [path that you need](https://overheid.io/documentatie/openkvk), and feed it to the client.

Features
--------

* PSR-4 autoloading compliant structure
* PSR-7 Request and Response structure
* Unit-Testing with PHPUnit
* Comprehensive Guides and tutorial
* Easy to use to any framework or even a plain php file


Requirements
--------

* PHP => 7.1
* cURL Extension
* json Extension
 
Installation
--------

```text
composer require freshheads/kvk-api-client
```

Usage
--------

Want to use existing requests? There's a factory for that!
```php
<?php
    // You can use a different endpoint by defining this in the constructor
    $requestFactory = new \EngineerAirhead\KvkClient\RequestFactory();
```

Create a new instance of the API Client with your own API key and our cURL based request handler.
Rather want to use something else? You can, by replacing the `HttpRequestHandler` with something else that implements the `\EngineerAirhead\KvkClient\HttpRequestHandlerInterface`.

```php
<?php
    $kvkClient = new \EngineerAirhead\KvkClient\ApiClient('[API_KEY]', new \EngineerAirhead\KvkClient\HttpRequestHandler());
```

Get a list of companies at the KVK. In case of any error, the client will throw an Exception.
```php
<?php
    try {
        $kvkResponse = $kvkClient->sendRequest($requestFactory->getAllOpenKvkRequest());
    } catch (\EngineerAirhead\KvkClient\ApiException $exception) {
        // Do what you must
    }
```

As specified by the overheid.io [API documentation](https://overheid.io/documentatie/openkvk), responses are in json format. So don't forget to decode the information before usage.

Don't want to use this package any more? We can accept that. Just make sure you replace this client with anything else that uses the `Psr\Http\Client\ClientInterface`.
You are, after all, free to make your own choices ;)