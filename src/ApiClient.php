<?php
declare(strict_types=1);

namespace EngineerAirhead\KvkClient;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Exception\InvalidArgumentException;
use Zend\Diactoros\Response;

class ApiClient implements ClientInterface
{
    /** @var string */
    private $apiKey;

    /** @var HttpRequestHandlerInterface */
    private $httpRequest;

    public function __construct(string $apiKey, HttpRequestHandlerInterface $httpRequest)
    {
        $this->apiKey = $apiKey;
        $this->httpRequest = $httpRequest;
    }

    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws ApiException
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        $this->httpRequest->setOption(CURLOPT_URL, (string)$request->getUri());
        $this->httpRequest->setOption(CURLOPT_RETURNTRANSFER, true);
        $this->httpRequest->setOption(CURLOPT_HTTPHEADER, [
            'ovio-api-key: ' . $this->apiKey
        ]);

        $result = $this->httpRequest->execute();

        if ($result === false) {
            $errorMessage = $this->httpRequest->getErrorMessage();
            $errorNumber = $this->httpRequest->getErrorCode();

            throw new ApiException('(' . $errorNumber . ') Unable to process KVK request: ' . $errorMessage);
        }

        json_decode($result);

        if ($jsonError = json_last_error() !== JSON_ERROR_NONE) {
            throw new ApiException('(' . $jsonError . ') Unable to process KVK response: ' . json_last_error_msg());
        }

        try {
            $response = (new Response())->withStatus(200);
            $response->getBody()->write($result);
        } catch (InvalidArgumentException $e) {
            throw new ApiException('Unable to create API response: ' . $e->getMessage());
        } catch (\InvalidArgumentException $e) {
            throw new ApiException('Unable to create API response: ' . $e->getMessage());
        } catch (\RuntimeException $e) {
            throw new ApiException('Unable to create API response: ' . $e->getMessage());
        }

        return $response;
    }
}