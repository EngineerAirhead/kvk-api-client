<?php
declare(strict_types=1);

namespace EngineerAirhead\KvkClient;

use Psr\Http\Message\RequestInterface;
use Zend\Diactoros\Request;

class RequestFactory
{
    /** @var string */
    private $baseUri;

    public function __construct(string $baseUri = 'https://api.overheid.io/')
    {
        $this->baseUri = $baseUri;
    }

    public function getAllOpenKvkRequest(
        $size = 100,
        $page = 1,
        $fields = ['handelsnaam', 'dossiernummer', 'subdossiernummer'],
        $filters = [],
        $query = '',
        $queryFields = []
    ): RequestInterface {
        $uri = $this->baseUri . 'openkvk/';

        $queryParams = ['size' => $size, 'page' => $page, 'query' => $query];

        if (count($fields) > 0) {
            $queryParams['fields'] = $fields;
        }

        if (count($filters) > 0) {
            $queryParams['filters'] = $filters;
        }

        if (count($queryFields) > 0) {
            $queryParams['queryfields'] = $queryFields;
        }

        return new Request($uri, 'GET');
    }

    public function getOpenKvKByIdRequest(string $id): RequestInterface
    {
        return new Request($this->baseUri . 'openkvk/' . $id, 'GET');
    }

    public function getSuggestOpenKvkRequest(string $query, int $size = 10, array $fields = []): RequestInterface
    {
        $uri = $this->baseUri . 'openkvk/' . $query;

        $queryParams = ['size' => $size];
        if (count($fields) > 0) {
            $queryParams['fields'] = $fields;
        }

        $uri .= '?' . http_build_query($queryParams);

        return new Request($uri, 'GET');
    }
}