<?php
declare(strict_types=1);

namespace EngineerAirhead\KvkClient;

interface HttpRequestHandlerInterface
{
    public function setOption($name, $value);

    public function execute();

    public function getInfo($name);

    public function close();

    public function getErrorMessage(): string;

    public function getErrorCode(): int;
}