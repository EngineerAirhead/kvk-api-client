<?php

use Freshheads\Client\ApiClient;
use Freshheads\Client\ApiException;
use Freshheads\Client\HttpRequestHandlerInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;

class ApiClientTest extends TestCase
{
    /** @var string */
    private $apiKey;

    /** @var ApiClient */
    private $client;

    /** @var HttpRequestHandlerInterface|ObjectProphecy */
    private $httpRequest;

    /** @var UriInterface|ObjectProphecy */
    private $uri;

    /** @var RequestInterface|ObjectProphecy */
    private $request;

    public function setUp()
    {
        $this->apiKey = '271536617d6baab8434622de7747414f2ef136d954658ba1df9ecbe8782be869';

        $this->httpRequest = $this->prophesize(HttpRequestHandlerInterface::class);
        $this->httpRequest->execute()->willReturn('{}');
        $this->httpRequest->setOption(CURLOPT_URL, 'http://www.test.nu')->shouldBeCalledTimes(1);
        $this->httpRequest->setOption(CURLOPT_RETURNTRANSFER, true)->shouldBeCalledTimes(1);

        $this->client = new ApiClient($this->apiKey, $this->httpRequest->reveal());

        $this->uri = $this->prophesize(UriInterface::class);
        $this->uri->__toString()->willReturn('http://www.test.nu');

        $this->request = $this->prophesize(RequestInterface::class);
        $this->request->getUri()->willReturn($this->uri->reveal());
        $this->request->getMethod()->willReturn('GET');
    }

    public function testSendRequestAddsApiKeyHeaderToHttpRequest()
    {
        $this->httpRequest->setOption(CURLOPT_HTTPHEADER, [
            'ovio-api-key: ' . $this->apiKey
        ])->shouldBeCalledTimes(1);

        $this->client->sendRequest($this->request->reveal());
    }

    public function testSendRequestThrowsApiExceptionWhenRequestFailed()
    {
        $this->httpRequest->execute()->willReturn(false);

        $this->httpRequest->setOption(CURLOPT_HTTPHEADER, [
            'ovio-api-key: ' . $this->apiKey
        ])->shouldBeCalledTimes(1);

        $this->httpRequest->getErrorCode()->willReturn(401);
        $this->httpRequest->getErrorMessage()->willReturn('NO U');

        $this->expectException(ApiException::class);

        $this->client->sendRequest($this->request->reveal());
    }
}
